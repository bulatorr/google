module gitlab.com/bulatorr/google

go 1.20

require (
	golang.org/x/crypto v0.11.0
	google.golang.org/protobuf v1.31.0
)

require golang.org/x/sys v0.10.0 // indirect
