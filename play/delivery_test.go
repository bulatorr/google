package play

import (
	"fmt"
	"os"
	"testing"
)

func Test_Delivery(t *testing.T) {
	home, err := os.UserHomeDir()
	if err != nil {
		t.Fatal(err)
	}
	var head Header
	head.Read_Auth(home + "/google/play/auth.txt")
	head.Read_Device(home + "/google/play/x86.bin")
	deliver, err := head.Delivery("com.google.android.youtube", 1524221376)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Printf("%+v\n", deliver)
}
