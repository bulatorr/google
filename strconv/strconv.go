package strconv

import (
	"encoding/hex"
	"net/url"
	"strconv"
	"unicode"
	"unicode/utf8"
)

type Cardinal float64

func (c Cardinal) String() string {
	units := []unit_measure{
		{1, ""},
		{1e-3, " thousand"},
		{1e-6, " million"},
		{1e-9, " billion"},
		{1e-12, " trillion"},
	}
	return scale(float64(c), units)
}

type Percent float64

func (p Percent) String() string {
	unit := unit_measure{100, "%"}
	return label(float64(p), unit)
}

type Rate float64

func (r Rate) String() string {
	units := []unit_measure{
		{1, " byte/s"},
		{1e-3, " kilobyte/s"},
		{1e-6, " megabyte/s"},
		{1e-9, " gigabyte/s"},
		{1e-12, " terabyte/s"},
	}
	return scale(float64(r), units)
}

type Size float64

func (s Size) String() string {
	units := []unit_measure{
		{1, " byte"},
		{1e-3, " kilobyte"},
		{1e-6, " megabyte"},
		{1e-9, " gigabyte"},
		{1e-12, " terabyte"},
	}
	return scale(float64(s), units)
}

// I considered mime/quotedprintable, but it does not have a Binary option for
// reading, which means that spaces would need to be encoded.
const escape_character = '%'

// I originally used:
// mimesniff.spec.whatwg.org#binary-data-byte
// but it fails with:
// fileformat.info/info/unicode/char/1b
func Binary_Data(r rune) bool {
	// this needs to be first because newline is a control character
	if unicode.IsSpace(r) {
		return false
	}
	// we could also use unicode.IsGraphic or unicode.IsPrint, but they call
	// unicode.In
	return unicode.IsControl(r)
}

func Decode(src string) ([]byte, error) {
	dst, err := url.PathUnescape(src)
	if err != nil {
		return nil, err
	}
	return []byte(dst), nil
}

func Encode(src []byte) string {
	var dst []byte
	for len(src) >= 1 {
		r, size := decode_rune(src)
		s := src[:size]
		if r == utf8.RuneError && size == 1 {
			var d [2]byte
			hex.Encode(d[:], s)
			dst = append(dst, escape_character)
			dst = append(dst, d[:]...)
		} else {
			dst = append(dst, s...)
		}
		src = src[size:]
	}
	return string(dst)
}

func decode_rune(p []byte) (rune, int) {
	r, size := utf8.DecodeRune(p)
	if r == escape_character || Binary_Data(r) {
		return utf8.RuneError, 1
	}
	return r, size
}

func label(value float64, unit unit_measure) string {
	var prec int
	if unit.factor != 1 {
		prec = 2
		value *= unit.factor
	}
	return strconv.FormatFloat(value, 'f', prec, 64) + unit.name
}

type unit_measure struct {
	factor float64
	name   string
}

func scale(value float64, units []unit_measure) string {
	var unit unit_measure
	for _, unit = range units {
		if unit.factor*value < 1000 {
			break
		}
	}
	return label(value, unit)
}
